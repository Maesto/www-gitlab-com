---
layout: markdown_page
title: "Sales Handbook"
---

## Communication<a name="reach-sales"></a>

- [**Public Issue Tracker**](https://gitlab.com/gitlab-com/sales/issues/); please use confidential issues for topics that should only be visible to team members at GitLab.
- You can also send an email to the Sales team via sales [at] gitlab.com
- [**Chat channel**](https://gitlab.slack.com/archives/sales); please use the `#sales` chat channel for questions that don't seem appropriate to use the issue tracker or the internal email address for.

## On this page

* [Pages inside Sales Handbook](#pages)
* [Pages outside Sales Handbook](#pages-outside)
* [GitLab Version Check](#version_check)
* [Parent and Child Accounts](#parent)
* [When to create an Opportunity](#opportunity)
* [Opportunity Naming Convention](#naming)
* [Opportunity Types](#types)
* [Opportunity Stages](#stages)
* [Processing Orders](#processing)
* [Closing A Won Deal](#closing)
* [Using Cases in Salesforce](#cases)
* [Process after you sell a Premium Support Subscription](#premium-support)
* [Export Control Classification, and Countries We Do Not Do Business In](#export-control)

## Pages inside Sales Handbook <a name="pages"></a>

* [Sales Onboarding](https://about.gitlab.com/handbook/sales-onboarding/)
* [Sales Training](https://about.gitlab.com/handbook/sales-training/)
* [Lead Qual & Demand Gen](https://about.gitlab.com/handbook/marketing/demand-generation/)
* [Sales Discovery Questions](https://about.gitlab.com/handbook/sales-qualification-questions/)
* [GitLab Positioning](https://about.gitlab.com/handbook/positioning-faq/)
* [FAQ from prospects](https://about.gitlab.com/handbook/sales-faq-from-prospects/)
* [Client Use Cases](https://about.gitlab.com/handbook/use-cases/)
* [Sales demo script](/handbook/sales/demo/)
* [Demo: Idea to Production](/handbook/sales/idea-to-production)

### Pages outside Sales Handbook <a name="pages-outside"></a>

* [Customer Reference Sheet](https://docs.google.com/spreadsheets/d/1Off9pVkc2krT90TyOEevmr4ZtTEmutMj8dLgCnIbhRs/edit#gid=0)
* [GitLab University](https://university.gitlab.com/)
* [Our Support Handbook](https://about.gitlab.com/handbook/support/)

### GitLab Version Check <a name="version_check"></a>

Before prospecting and engaging with a prospect, check to see if they are using CE. To do this, use GitLab Version Check. Everything about [GitLab Version Check](/handbook/sales-process/version_check).

### Parent and Child Accounts <a name="parent"></a>

* A Parent account is the business/organization which owns another business/organization.  Example: Disney is the parent account of ESPN and ABC.
* A Child Account is the organization you may have an opportunity with but is owned by the Parent Account.
* You may have a opportuity with the Parent account and a Child Account.  Example: Disney and ESPN may both be customers and have opportunities.
* When selling into a new division (which has their own budget, different mailing address, and decision process) create a new account.  This is the Child Account.  For every child account, you must select the parent account by using the parent account field on the account page.
* We want to do this as we can keep each opportunity with each child account separate and easily find all accounts and opportunities tied to a parent account.

### When to create an Opportunity <a name="opportunity"></a>

Before a lead is converted or an opportunity is created the following must occur:

* Identified problem GitLab can solve - happens during introductory call
* Interest by prospect to learn more about GitLab - moving to discovery stage
* Discovery call scheduled with prospect - moving to discovery stage
* Interest by GitLab salesperson to pursue the opportunity
* If you have given a quote (pricing) please convert as this is now a sales opportunity

### Opportunity Naming Convention <a name="naming"></a>

Opportunities will follow the following rules:

* Subscription: [name of company] [type of subscription] [number of packs]. Example: MicahTek, Inc.-Standard-1
* Training: [name of company] [type of training: User Training/Git Workshop/GitLab Flow/GitLab Admin].
Example: First Community Credit Union User-Training. **Note** to set up the actual training,
follow the workflow described on the [Support handbook](https://about.gitlab.com/handbook/support/).
* Any deal coming from Gitorious has “(from Gitorious)” added.  Example MicahTek, Inc.-Gitorious-Standard-1

### Opportunity Types <a name="types"></a>

1. New Business - This type should be used for any new account (business) who signs up. Paid training also falls under this type if the organization does not have an enterprise license.
1. Web Direct -  This type should be used for any new business who pays directly online
1. Add-on - This type should be used for any incremental/upsell business sold into an existing account and division mid term, meaning not at renewal.
1. Exisiting Account (new divsion) - This type should be used for new business sold into an existing account but a new division, a new puchasing group.
1. Renewal - This type should be used for an existing account renewing their license with GitLab.  Renewals can have their value increased, decreased or stay the same.  We capture growth/loss as a field in Salesforce.com

### Opportunity Stages <a name="stages"></a>

1. Discovery - research, fact finding, identify contacts, current situation,
1. Developing - isolate the opportunities, assess needs, qualify the opportunities, build rapport, access decision makers, understand decision process and criteria, Collaborate on solutions, define the business case, assess competition
1. Present Solution - Tailor the presentation, coordinate the team, present recommendations, present pricing, isolate value proposition, gain feedback
1. Negotiating - negotiate business terms resolve objections, set close plan (sequence of events), gain commitment
1. Verbal Commitment - how a company approves s business teams desire to purchase, how the company vets purchases from a legal, info security, risk, compliance and vendor management perspective
1. Won - Deal won and booked
1. Lost - Opportunity not won at this time

### Processing Orders via Zuora <a name="processing"></a>

#### Step 1 - New Account
1. Click on the Accounts tab
1. Click on New button to Create Account
1. Create Account Screen – Enter all mandatory fields and click on Save – Account is created
1. A specific custom field has been created to capture the Entity information.
 * NL
 * US
1. Sales rep will choose the Entity from the Dropdown
 
#### Step 2 - Contacts
1. Create Contacts, by clicking on the new contact button
1. Make sure to add in the address for any contacts associated with the quote (Bill To and Sold To)
 
#### Step 3 -  Opportunity
1. Create Opportunity by navigating to the Opportunity Related list and click on New button
1. Enter all the required fields based on your business process and click on Save Button
 
#### Step 4 - Zquotes – New QUOTE
1. Click on New Quote button within the opportunity
1. Enter all details and click on Next and select products to complete the Quote creation process
1. All the fields marked in red are mandatory fields, below are fields description
1. Valid Date – Date until when Quote is valid; This is auto set to 30 days from today’s date
1. Quote Template – click on the button to select the Quote template
1. Bill To contact  - Click on the vlookup button to enter the Bill To contact information. This will bring up a pop-up window that will list all the contact that were created during Step 2 of the process. Bill To person would be the contact to whom the bill will be sent
1. Sold To Contact - Click on the vlookup button to enter the Sold To contact information. This will bring up a pop-up window that will list all the contact that were created during Step 2 of the process. Sold To person would be the contact to whom the product was sold
1, Invoice Owner - 
* This field will only be used incase of creating a Quote created for a End Customer that involves a Partner; Please see Creating a Quote for Partner Section
1. Payment method -  refers to the type of payment the customer is using for paying this Quote/Subscription
1. Payment Methods currently defined are as follows –
 * Credit card
 * ACH
 * Check
 * Wire transfer
1. Currency - by default below are the fields available, the currencies can be removed/Added or hidden based on your business needs
 * USD
 * EUR
1. Click Thru EULA - is used when an agreement has not been signed; A use case being a product(EE ) is sold through a partner
1. Start Date -  Specify the date on which this subscription, or contract, is effective. This date becomes the Contract Effective Date of the subscription in Zuora.
1. Subscription Term Type:
 * By default set as Termed
1. Initial Term and Renewal Term -  Specify initial term and the renewal term in months
1. Auto Renew -  it is checked by Default; Subscription automatically renew when the initial term for a subscription is over.
1. Special Terms and Notes - Enter any additional notes that is not specified by the above settings. 
1. Click on Next to make the product selection
1. Product Selector Page
1. Click on Product Vlookup Button to make the product selection
1. Click on Rate plan Vlookup Button to make the rate plan selection
 * GitLab Enterprise Edition is the rate plan when selling per license seats @ $39
1. Once the quote has been created, you can modify it, delete it, or send it from Salesforce to Z-Billing to create a subscription.
 
#### Step 5 – Generate PDF
1. Upon creation of a Quote, click Generate PDF.  The PDF will be saved as an attachment under the opportunity object.

#### Step 6 – Send for signature via Sertifi
1. Within the ZQuote object, click Sertifi E-Sign button near the top of page.
1. 1st signer will be auto-populated with the bill to contact.  You can change if needed.  
1. Add in the 2nd signer and click add participant. Second signer is GitLab executive. Click Next
1. You will now be asked to attach the Quote PDF.  Under the drop down for Related Notes and Attachments.  Select the quote for signature and click send for signature.
1. Once all parties have signed, the fully executed document will be saved as an attachment.

#### Step 7 – Send to Z-billing
1. Once the Quote is signed, Sales rep will click on Send to Z-billing button to send the Quote to Zuora

##### Quote Metrics
The following quote metrics are available:
 * MRR (Monthly Recurring Revenue) - Monthly recurring revenue (MRR) calculates subscription recurring fees normalized to a monthly value.
 * Delta MRR - Delta of the total MRRs between the original subscription and the amendment.
 * TCV (Total Contract Value) - Total contract value (TCV) calculates the total recurring charges over the lifetime of a subscription.
 * Delta TCV - Delta of the TCVs between the original subscription and the amendment.
 * Sub-total - The quote subtotal for the specified billing periods, excluding discount charges and taxes.
 * Discount - The sum of all discount charges for the specified billing periods.
 * Tax - The tax calculated for the specified billing periods.
 * Total - The quote total for the specified billing periods, including discount charges and taxes.
       
##### Edit Quote Button
 * You can only edit a quote before it is sent to Z-Billing. After a quote is sent to Z-Billing and its subscription is created, you can no longer edit the quote in Salesforce.

##### Edit a Quote
 * To Edit a Quote, click Edit Quote Details.
 * On Edit Quote Details page, make the desired changes.
 * Click Save

##### Select products Button
 * To add, remove, or update products, rate plans, and charges for a quote sales rep can click Select Products.
 * Make necessary changes to products, rate plans, and charges
 * Click Save.

##### Delete Button
 * On the Quote Detail page, click Delete Quote Details to delete a quote that was created.
 * A message would appear to confirm on delete on click of ok quote gets deleted
 
##### Generate PDF Button
 * This will allow the sales rep to Generate a quote pdf
 * On click of Generate PDF – Quote PDF gets generated 

#####  Send to Z-Billing Button
 * On the Order Preview page that opens, review the information, and then click Submit to send the quote to Z-Billing.
 * A confirmation popup shows up, Zuora Quotes has successfully sent your quote to the Z-Billing and a subscription was created

#### Step 8 – Close Win and Opportunity
1. Once quote is signed and attached to opportunity, via Sertifi or manually and quote has been successfully sent to z-billing, go into the opportunity and change the stage to closed won.
1. A renewal opportunity will be automatically created for the account


### Returning Customer Creation Process(Upgrade/Renewals/Cancellations)
1. Create an Opportunity for an Add-on or Renewal 
1. Once an opportunity is created click on New Quote button within the opportunity
1. Since this is a returning customer, sales rep would see the following screen
1. This screen determines that this customer is already established in Zuora and will allow the sales rep to perform on the 4 actions –

#### New Subscription for the account
 * Amend existing subscription for this billing account
 * Renew existing subscription for this billing account
 * Cancel existing subscription for this billing account
 
##### New Subscription for the account
1. This will allow the Sales rep to create a new subscription for the existing billing account.
1. Clicking on Next will take the sales rep thru the same Quoting flow that was seen the new Quote creation process

##### Amend existing subscription for the billing account
1. This process is used to perform any Upgrades on an existing subscription
1. Choosing “Amend existing subscription for billing account”, will allow sales rep to perform amendment to an existing subscription.(Upgrades)
1. Clicking on the this radio button will list all subscriptions that are tied to the customer
1. Click to choose the subscription for performing an Amendment and hit on Next button

#### Zuora Supporting 4 types of Amendments –
1. Terms and Conditions amendment – The sales rep will be able to change the terms and conditions of an existing subscription;
1. Remove product Amendment – The sales rep will be be able to perform a Remove a product Amendment;
 * In this case, sales rep will have to Set the Start date(Contract effective date in Zuora terms) when the remove product amendment should happen
 * Click on Next
 * This will take them to the product selector page and displays the original product that was purchased within the subscription
 * Sales rep can now remove the product
1. Add product Ammendment – Sales rep can add a new product from the product selector page
1. Update Product Amendment – Sales rep can update the existing product of the existing selected product
 * Note: Do not change the Terms and conditions unless you are performing a terms and conditions amendment(except for start date).

##### Let’s take an Example – Let’s say a sales rep is performing an upgrade to a customer from Basic to Standard product
This will include 3 steps –
1. Set the Start date
1. Remove the basic product
1. Add the Standard product
 * Hit on Save

Once on the Quote Summary, will click on generate PDF to generate a Quote PDF
Send it to the customer - only if there is no record of a signed quote.
Upon Sign-off, or existing signed quote, click on the Send to Z-billing button to send the Quote over to Zuora
 
Renew existing subscription for the billing account
1. This process is used to perform Renewal on an existing subscription; this is only created if the AUTO RENEW Flag is set to “NO” for a subscription initially.
1. Choosing “Renew existing subscription for billing account”, will allow sales rep to perform Renewal to an existing subscription;
1. Clicking on the this radio button will list all subscriptions that are tied to the customer
1. Clicking on next will take the sales to the Create Renewal Quote page
1. Sales rep will select the renewal Quote Template from the list
1. Enter the Renewal term in months
1. Will hit on Next
1. Skip the product selector page, unless want to update the QTY or want to add a new product
1. Once on the Quote Summary, will click on generate PDF to generate a Quote PDF
1. Send it to the customer
1. Upon Sign-off will click on the Send to Z-billing button to send the Quote over to Zuora
 
Cancel existing subscription for the billing account
1. This process is used to perform Cancellation of an existing subscription;
1. Choosing “Cancel existing subscription for billing account”, will allow sales rep to perform cancellation to an existing subscription;
1. Clicking on the this radio button will list all subscriptions that are tied to the customer
1. Set the Start Date(Contract effective date) of when the customer have request for cancellation
1. Choose a Cancellation Effective date
1. Hit on next button
1. Once on the Quote Summary, will click on generate PDF to generate a Quote PDF
1. Send it to the customer
1. Upon Sign-off will click on the Send to Z-billing button to send the Quote over to Zuora
 
### Creating a Quote for a Partner Use-case using the Invoice Owner Functionality - 
 * Note - We can make use of Invoice owner functionality so that subscription can be billed to the Partner and the subscription would be owned by the End Customer; 
 * Note - if the Partner is a getting created the first time in SF, then please make sure this is also created in Zuora; And partner account(in SF and Zuora) are mapped using the CRM iD in Zuora.
Please see screenshot below - 

#### Step 1 - Check if the Partner has an Associated Billing Account
1. Salesrep to see if the partner account is in SF and scroll down to see if there is a mapped Billing Account for the customer Account; please see below 
 * If yes, Sales rep will then start creating the End Customer details
1. Salesrep to create an End Customer Account in SF
1. Creates associated Contact
1. Creates Opportunity
 * If no, Sales rep to create a partner Account + a Billing Account in Zuora

#### Step 2 - Create a Quote for the End Customer
1. Sales rep will create a Quote for the End Customer
1. On creation of Quote using ZQuotes, sales rep should populate the invoice owner field (shown above)
1. Upon clicking on the field sales rep will be listed with all Billing Accounts from Zuora. 
1. Sales rep will select the right partner name since the partner is already established in Zuora
1. Clicks on Next and selects the products and click on Next

### Closing a Won Deal <a name="closing"></a>

In order to close a deal in Salesforce.com, one of the following must happen:

1. Client paid via Credit Card
1. Prospect has returned a signed quote (attach to the opportunity). Quote is needed for non online purchases to confirm what was purchased, the effective dates and the cost.  Quote is also needed to confirm they agree to terms and conditions.  

Once the above has happened, please make sure the following actions are done:

1. Attach any signed agreement, PO, and/or quote as an attachment to the account page/object in Salesforce.com
1. Add in the competition we were up against in the opportunity.  This can be the legacy SCM solution they had and/or who they were evaluating along side of us. This can also be ourselves, meanign GitLab CE or .com
1. Create an add-on or Existing Account (new division) opportunity if there has been one identified by you at this time.
1. An automated task is created to check in with the Account, 30 days after the deal closes.  The purpose for this check in is to make sure they are happy, address any questions they have, ask them to be a reference or to speak with our product marketing team on how they use GitLab.
  * If the customer agrees to be a reference, please check the box on the account object called "referenceable customer".  Also go to the contact object who agreed to be a reference and under the field "role" please select "reference - investors and prospects"
  * If customer agrees to speak with product marketing about how they use GitLab, please email product marketing manager
1. Once the opportunity is closed won, the field "type" on the account object will change to "customer"
1. A task will be auto created in Salesforce.com reminding you to update the Technology Stack field on the account to reflect the GitLab Tools they are using. Example: GitLab Issue Tracker, CI, Wiki

### Using Cases in Salesforce <a name="cases"></a>

A case is a question or feedback from a prospect or customer. Each of us can review cases to see how we can deliver better service.

When a case is submitted, if the contacts email address is in salesforce, the contact owner will be assigned to the case.  If there is mo match, the case will go to our [BDR Queue](https://na34.salesforce.com/500?fcf=00B610000042ioq). Anyone can check this queue and reasssing to yourself if this is your contact or respond to this case.

Cases display in an Emails related list. The Emails related list includes all emails sent by your customer regarding a particular case, as well the email threads between you and your customer. The first 77 characters of an email message appear in the related list so that you can see what the message is about without having to click on it.

#### To work with Email-to-Case or On-Demand Email-to-Case emails:
* Click Send An Email to send an email to a contact, another user, or any other email address. Select a "From Address".  Preferrably use sales@ address.
* Click Reply to respond to an email. The email response automatically includes the email body as received from the customer. Enter your response and click Send.
* Click To All to respond to all participants on an email thread.
* The email address of the contact who created the case automatically appears in the To field, as long as the creator is an existing contact. To add more recipients, click Lookup icon to look up an address, or type or paste email addresses or names in the To field.
* When you enter an email address or name that matches one contact or user, the address appears as a button with the person’s name.
* To add several addresses at once, copy and paste them separated by spaces or commas. These email addresses appear as buttons, and, if they’re associated with a contact or user, show the contact’s name.
* When you enter an email address that matches multiple contacts or users, the address appears as a button. Clicking the button brings up a list of people associated with the email address so you can choose the one you want to associate with the message.
* If you need to copy other people on the message, click Add Cc or Add Bcc.
* By default, the email subject is the name of the case it’s related to. You can edit the subject if you need to.

* Click the subject of the email to view the email. From the email, you can reply to the sender, reply to everyone, forward the email, or delete it.
* While viewing an email, you can display a list of all the emails associated with the case by clicking Email Message List, and you can navigate to the case's other emails by clicking Next or Previous.
* While viewing an email, click Forward to forward it. The email automatically includes the email body as received from the customer. Optionally, enter text and click Send.

#### Closing a Case
* Before closing a case, make sure that the account is associated with the case
* Choose the case reason before you close

### Process after you close a Premium Support Subscription <a name="premium-support"></a>

Once you close a Premium Support Subscription deal, you need to do a few things.

1. When possible, create an issue on dev.gitlab.org under Organization with the Support label as soon as it is clear that a dedicated service engineer will be needed. Service Engineers can volunteer, or they will be assigned by VP Engineering if nobody is volunteering. It can depend on the client, and factors like: timezone, language, specific needs, etc.
Once this is determined, please add the Dedicated Service Engineer to the Salesforce account with the drop down.
1. The next step is to introduce your client to their Dedicated Engineer, and if necessary, work together to schedule an on-boarding call to cover any questions, and give an overview of the support they are entitled to.
1. You would have probably brought this up prior to the deal closing, but if the client is interested in joining the Slack support channel, please work with the Dedicated Engineer to make this happen.

### Export Control Classification, and Countries We Do Not Do Business In <a name="export-control"></a>

GitLab's Export Control Classification (or ECCN) is 5D002.c.1
with CCATS number G163509.
This means that GitLab source code can be exported and re-exported under the
authority of license exception TSU of section [740.13(e)](https://www.bis.doc.gov/index.php/forms-documents/doc_view/986-740) of the export administration regulations (EAR).

Per [740.13(e)(2)(ii)](https://www.bis.doc.gov/index.php/forms-documents/doc_view/986-740) of the EAR, there are restrictions on "Any knowing export or reexport
to a country listed in [Country Group E:1 in Supplement No. 1 to part 740 of the EAR](https://www.bis.doc.gov/index.php/forms-documents/doc_download/944-740-supp-1)".

As a consequence of this classification, we currently do not do business in:
Iran, Sudan (excluding South Sudan), Syria, North Korea, and Cuba.
