---
layout: markdown_page
title: "Marketing"
---

## Welcome to the GitLab Marketing Handbook
{: .no_toc}

The GitLab Marketing team includes four functional groups: Demand Generation, Developer Relations, Design, & Product Marketing
{: .note}

----

## On this page
{: .no_toc}

- TOC
{:toc .toc-list-icons}

----

## <i class="fa fa-book fa-fw icon-color font-awesome" aria-hidden="true"></i> Marketing Handbooks
{: #marketing-handbooks}

- [Blog]
- [Demand Generation]
   - [Business Development]
   - [Field Marketing]
   - [Marketing Operations]
   - [Online Marketing]
- [Design]
- [Developer Relations]
   - [Developer Advocacy]
   - [Technical Writing]
       - [Markdown Guide]
- [Product Marketing]
   - [Content Marketing]
   - [Partner Marketing]
- [Social Media Guidelines]

----

## <i class="fa fa-gitlab fa-fw icon-color font-awesome" aria-hidden="true"></i> GitLab Marketing Mission
{: #mission}

<br>

<div class="alert alert-purple center"><h3 class="purple"><strong>GITLAB FOR ALL</strong></h3></div>

We think GitLab (.com, CE, and EE) can help developers, designers, IT workers, marketers, and everyone in between improve collaboration.

The GitLab marketing team is composed of a unique set of individuals from interesting backgrounds all over the world. Because of our unique skill sets, we're able to accomplish a lot of high impact campaigns through collaboration and open feedback from our team and the community.

## <i class="fa fa-users fa-fw icon-color font-awesome" aria-hidden="true"></i> Marketing Team Functional Groups
{: #groups}

Our Marketing team is lead by [Ashley Smith][ashley] - [Chief Marketing Officer (CMO)][cmo]. It is split into four key functional groups, which are equally important for the success of GitLab:

<!-- The following HTML blocks are the Functional Groups Boxes -->
<!-- DON'T EDIT THIS PART BELOW UNLESS YOU KNOW WHAT YOU'RE DOING :) -->
<div class="row mkt-row">
  <a href="" data-toggle="modal" data-target="#demand-gen-modal">
    <div class="col-sm-6 col-md-3 mkt-box">
      <div class="thumbnail">
        <img src="/images/icons/pages-icons/step-1-icon@3x.png" alt="GitLab Marketing - Demand Generation - Icon">
        <div class="caption">
          <p class="center description">
            <h4 class="purple">DEMAND GENERATION</h4>
          </p>
        </div>
      </div>
    </div>
  </a>
  <a href="" data-toggle="modal" data-target="#design-modal">
    <div class="col-sm-6 col-md-3 mkt-box">
      <div class="thumbnail">
        <img src="/images/icons/pages-icons/step-5-icon@3x.png" alt="GitLab Marketing - Design - Icon">
        <div class="caption">
          <p class="center description">
            <h4 class="purple">DESIGN</h4>
          </p>
        </div>
      </div>
    </div>
  </a>
  <a href="" data-toggle="modal" data-target="#dev-rel-modal">
    <div class="col-sm-6 col-md-3 mkt-box">
      <div class="thumbnail">
        <img src="/images/icons/pages-icons/step-2-icon@3x.png" alt="GitLab Marketing - Developer Relations - Icon">
        <div class="caption">
          <p class="center description">
            <h4 class="purple">DEVELOPER RELATIONS</h4>
          </p>
        </div>
      </div>
    </div>
  </a>
  <a href="" data-toggle="modal" data-target="#prod-mark-modal">
    <div class="col-sm-6 col-md-3 mkt-box">
      <div class="thumbnail">
        <img src="/images/icons/pages-icons/what-is-icon-1@3x.png" alt="GitLab Marketing - Product Marketing - Icon">
        <div class="caption">
          <p class="center description">
            <h4 class="purple">PRODUCT MARKETING</h4>
          </p>
        </div>
      </div>
    </div>
  </a>
</div>

{::options parse_block_html="true" /}

<!-- The following HTML code blocks are the Functional Group Modal Windows. HTML code cannot be properly intended
as we are using the markdown parser to mix it up with markdown. Intending will generate code blocks instead. -->
<!-- EDIT JUST THE MARKDOWN PARTS :) -->

<!-- Demand Gen -->
<div class="modal fade" id="demand-gen-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-header">
<h2 class="modal-title" id="myModalLabel">Demand Generation</h2>
</div>
<div class="modal-body">
<!-- EDIT FROM HERE ON LIKE NORMAL MARKDOWN -->

What is Demand Generation (to be included).

[Job Description](/jobs/demand-generation-manager/){:.btn .btn-purple-inv}
[Handbook][Demand Generation]{:.btn .btn-purple}
[<i class="fa fa-gitlab" aria-hidden="true"></i> Joe][joe]{:.btn .btn-orange}
[<i class="fa fa-gitlab" aria-hidden="true"></i> Hank][hank]{:.btn .btn-orange}

### Business Development
{: .no_toc}

What is BD (to be included).

[Job Description](/jobs/business-development-representative/){:.btn .btn-purple-inv}
[Handbook][Business Development]{:.btn .btn-purple}
[<i class="fa fa-gitlab" aria-hidden="true"></i> Braden][braden]{:.btn .btn-orange}
[<i class="fa fa-gitlab" aria-hidden="true"></i> Colton][colton]{:.btn .btn-orange}
[<i class="fa fa-gitlab" aria-hidden="true"></i> Ryan][ryan]{:.btn .btn-orange}

### Field Marketing
{: .no_toc}

What is Field Marketing (to be included).

[Job Description](/jobs/field-marketing-manager){:.btn .btn-purple-inv}
[Handbook][Field Marketing]{:.btn .btn-purple}
[<i class="fa fa-gitlab" aria-hidden="true"></i> Emily][emily]{:.btn .btn-orange}

### Marketing Operations
{: .no_toc}

What is Marketing Operations (to be included).

[Job Description](/jobs/marketing-operations-manager/){:.btn .btn-purple-inv}
[Handbook][Marketing Operations]{:.btn .btn-purple}

### Online Marketing
{: .no_toc}

What is Online Marketing (to be included).

[Job Description](/jobs/online-marketing-manager/){:.btn .btn-purple-inv}
[Handbook][Online Marketing]{:.btn .btn-purple}
[<i class="fa fa-gitlab" aria-hidden="true"></i> Mitchell][mitchell]{:.btn .btn-orange}

<!-- DON'T EDIT THIS PART BELOW UNLESS YOU KNOW WHAT YOU'RE DOING :) -->
</div>
<div class="modal-footer">
<button type="button" class="btn btn-purple" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i></button>
</div>
</div>
</div>
</div>

<!-- Design -->
<div class="modal fade" id="design-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-header">
<h2 class="modal-title" id="myModalLabel">Design</h2>
</div>
<div class="modal-body">
<!-- EDIT FROM HERE ON LIKE NORMAL MARKDOWN -->

Short description: what does Design does for GitLab (to be included).

[Job Description](/jobs/designer/){:.btn .btn-purple-inv}
[Handbook][Design]{:.btn .btn-purple}
[<i class="fa fa-gitlab" aria-hidden="true"></i> Luke][luke]{:.btn .btn-orange}

<!-- DON'T EDIT THIS PART BELOW UNLESS YOU KNOW WHAT YOU'RE DOING :) -->
</div>
<div class="modal-footer">
<button type="button" class="btn btn-purple" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i></button>
</div>
</div>
</div>
</div>

<!-- Dev Rel -->
<div class="modal fade" id="dev-rel-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-header">
<h2 class="modal-title" id="myModalLabel">Developer Relations</h2>
</div>
<div class="modal-body">
<!-- EDIT FROM HERE ON LIKE NORMAL MARKDOWN -->

What is Developer Relations (to be included).

### Developer Advocacy
{: .no_toc}

What is Dev. Adv. (to be included).

[Job Description](/jobs/developer-advocate){:.btn .btn-purple-inv}
[Handbook][Developer Advocacy]{:.btn .btn-purple}
[<i class="fa fa-gitlab" aria-hidden="true"></i> Amanda][amanda]{:.btn .btn-orange}
[<i class="fa fa-gitlab" aria-hidden="true"></i> Ivan][ivan]{:.btn .btn-orange}

### Technical Writing
{: .no_toc}

> _Technical writing is sometimes defined as simplifying the complex. In a concise and deceptively simple definition is a whole range of skills and characteristics that address nearly every field of human endeavor at some level._ ([techwhirl.com])

At GitLab, tech writers are the folks who take care of writing and maintaining technical content clear, concise, consistent, professional and up-to-date.

[Job Description](/jobs/technical-writer/){:.btn .btn-purple-inv}
[Handbook][Technical Writing]{:.btn .btn-purple}
[<i class="fa fa-gitlab" aria-hidden="true"></i> Axil][axil]{:.btn .btn-orange}
[<i class="fa fa-gitlab" aria-hidden="true"></i> Marcia][marcia]{:.btn .btn-orange}

<!-- DON'T EDIT THIS PART BELOW UNLESS YOU KNOW WHAT YOU'RE DOING :) -->
</div>
<div class="modal-footer">
<button type="button" class="btn btn-purple" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i></button>
</div>
</div>
</div>
</div>

<!-- Product -->
<div class="modal fade" id="prod-mark-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-header">
<h2 class="modal-title" id="myModalLabel">Product Marketing</h2>
</div>
<div class="modal-body">
<!-- EDIT FROM HERE ON LIKE NORMAL MARKDOWN -->

What is Product Marketing (to be included)

[Job Description](/jobs/product-marketing-manager){:.btn .btn-purple-inv}
[Handbook](/handbook/marketing/product-marketing){:.btn .btn-purple}
[<i class="fa fa-gitlab" aria-hidden="true"></i> Amara][amara]{:.btn .btn-orange}

### Content Marketing
{: .no_toc}

What is Content Marketing (to be included)

[Job Description](/jobs/content-marketing-manager){:.btn .btn-purple-inv}
[Handbook][Content Marketing]{:.btn .btn-purple}

### Partner Marketing
{: .no_toc}

What is Partner Marketing (to be included)

[Job Description coming soon](#){:.btn .btn-purple-inv}
[Handbook][Partner Marketing]{:.btn .btn-purple}

<!-- DON'T EDIT THIS PART BELOW UNLESS YOU KNOW WHAT YOU'RE DOING :) -->
</div>
<div class="modal-footer">
<button type="button" class="btn btn-purple" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i></button>
</div>
</div>
</div>
</div>

{::options parse_block_html="false" /}
<!-- END MARKDOWN + HTML PARSER -->

## <i class="fa fa-suitcase fa-fw icon-color font-awesome" aria-hidden="true"></i> Marketing Production
{: #marketing-products}

<!-- The following HTML blocks are the Marketing Products boxes -->
<!-- DON'T EDIT THIS PART BELOW UNLESS YOU KNOW WHAT YOU'RE DOING :) -->
<div class="row mkt-row">
  <a href="/blog/">
    <div class="col-sm-6 col-md-3 mkt-box">
      <div class="thumbnail product">
        <img src="/images/icons/blog.png" alt="GitLab Marketing - Demand Generation - Icon">
        <div class="caption">
          <p class="center description">
            <h4 class="orange">BLOG</h4>
          </p>
        </div>
      </div>
    </div>
  </a>
  <a href="https://gitlab.mybrightsites.com/">
    <div class="col-sm-6 col-md-3 mkt-box">
      <div class="thumbnail product">
        <img src="/images/icons/swag_shop.png" alt="GitLab Marketing - Design - Icon">
        <div class="caption">
          <p class="center description">
            <h4 class="orange">SWAG</h4>
          </p>
        </div>
      </div>
    </div>
  </a>
  <a href="/blog/categories/events/">
    <div class="col-sm-6 col-md-3 mkt-box">
      <div class="thumbnail product">
        <img src="/images/icons/location.png" alt="GitLab Marketing - Developer Relations - Icon">
        <div class="caption">
          <p class="center description">
            <h4 class="orange">EVENTS</h4>
          </p>
        </div>
      </div>
    </div>
  </a>
  <a href="/handbook/marketing/developer-relations/developer-advocacy/">
    <div class="col-sm-6 col-md-3 mkt-box">
      <div class="thumbnail product">
        <img src="/images/icons/news.png" alt="GitLab Marketing - Product Marketing - Icon">
        <div class="caption">
          <p class="center description">
            <h4 class="orange">TALKS</h4>
          </p>
        </div>
      </div>
    </div>
  </a>
</div>
<!-- NEXT ROLL OF 4 -->
<div class="row mkt-row">
  <a href="http://doc.gitlab.com/">
    <div class="col-sm-6 col-md-3 mkt-box">
      <div class="thumbnail product">
        <img src="/images/icons/documentation.png" alt="GitLab Marketing - Demand Generation - Icon">
        <div class="caption">
          <p class="center description">
            <h4 class="orange">DOCS</h4>
          </p>
        </div>
      </div>
    </div>
  </a>
  <a href="/">
    <div class="col-sm-6 col-md-3 mkt-box">
      <div class="thumbnail product">
        <img src="/images/icons/website.png" alt="GitLab Marketing - Design - Icon">
        <div class="caption">
          <p class="center description">
            <h4 class="orange">WEBSITE</h4>
          </p>
        </div>
      </div>
    </div>
  </a>
  <a href="https://www.youtube.com/playlist?list=PLFGfElNsQthZnwMUFi6rqkyUZkI00OxIV">
    <div class="col-sm-6 col-md-3 mkt-box">
      <div class="thumbnail product">
        <img src="/images/icons/team_alt.png" alt="GitLab Marketing - Developer Relations - Icon">
        <div class="caption">
          <p class="center description">
            <h4 class="orange">WEBCASTS</h4>
          </p>
        </div>
      </div>
    </div>
  </a>
  <a href="https://university.gitlab.com/">
    <div class="col-sm-6 col-md-3 mkt-box">
      <div class="thumbnail product">
        <img src="/images/icons/training.png" alt="GitLab Marketing - Product Marketing - Icon">
        <div class="caption">
          <p class="center description">
            <h4 class="orange">UNIVERSITY</h4>
          </p>
        </div>
      </div>
    </div>
  </a>
</div>
<!-- END OF MARKETING PRODUCTS -->

## <i class="fa fa-clock-o fa-fw icon-color font-awesome" aria-hidden="true"></i> Marketing OKR Overview
{: #okrs}

Our team and the demands on marketing are growing quickly. In order to align our goals with company goals as well as prioritize what we are working on, OKRs help us to maintain structure. Of course, not everything can be captured in one Google Sheet but this helps us all to know what we consider our goals as a team to be.  

Each member of the marketing team is responsible for 3 Objectives and 3 Key Results for each objective.  

To find the GitLab Marketing Team's OKRs, search the company Google Drive for "Marketing OKRs - Quarterly".

- What is an OKR? **Objectives and Key Results**
- Objectives are goals
- Key Results are measurement
- OKRs help to prioritize and align
- OKRs are set quarterly
- OKRs are NOT a measurement of performance.
- Set goals that aren’t easy to hit.
- Graded on a scale of 0.0 to 1.0. Normal is around .8
- [Quick overview of OKRs](http://www.slideshare.net/HenrikJanVanderPol/how-to-outperform-anyone-else-introduction-to-okr)


### Team OKRs
{: .no_toc}

<br>

<figure class="video_container">
<iframe src="https://docs.google.com/spreadsheets/d/1JpruAErYC1d_NuJjOfz98tArn8YDIwLJXrSWbt-hMyA/pubhtml?widget=true&amp;headers=false"></iframe>">
</figure>

<br>

## <i class="fa fa-user fa-fw icon-color font-awesome" aria-hidden="true"></i> Hiring Plan


<br>

<figure class="video_container">
<iframe src="https://docs.google.com/spreadsheets/d/1Zye1AE9_ouAAiNmugD4uT03fUHC_V3rxvnZY7fa0kBE/pubhtml?widget=true&amp;headers=false"></iframe>
</figure>

<br>


## <i class="fa fa-cogs fa-fw icon-color font-awesome" aria-hidden="true"></i> Meetings and structure
{: #meetings}

These are just the required meetings for team members and managers. Of course, meetings are encouraged when it expedites a project or problem solving amongst member so the team and company. Don't be afraid to say "Hey, can we hangout?" if you need help with something.

### Weekly 1:1 (New hire: First month with all direct reports)
{: .no_toc}

**Meeting goal: For manager to help new team member onboard quickly.**  

**Run time: 30 minutes**

All managers should have a weekly 1:1 with their direct reports in the first month of employment, starting with the first day of employment where possible.  

The first meeting should run as follows:  

- Welcome and make sure new hire is working through onboarding tasks.  
- Suggest people on the marketing team and beyond to meet with.  
- Manager should make sure all technology needs are taken care of.  
- Manager should answer or help to find proper resource for any questions.  
- Manager should create Google Doc private 1:1 agenda for all recurring 1:1's and add to the description of the calendar invite.  

The agenda of the following 1:1s should be the same as the recurring Bi-weekly 1:1s with time set aside to answer any questions about onboarding.


### Bi-weekly 1:1 (After first month with all direct reports)
{: .no_toc}

**Meeting goal: For manager to remove roadblocks and help prioritize so team member can be effective.**  

**Run time: 30 minutes**

All managers should have twice monthly (bi-weekly) meetings with all of his or her direct reports.

The meeting should run as follows:

- Always add agenda items to Google Doc agenda in description of meeting invite.
- First discuss any issues or problems that the manager can help with. Any roadblocks or disagreements?
- Go through the agenda and answer any questions.
- Action items for the employee should be marked in Red.
- Action items for the manager should be marked in Blue.
- All action items should be handled before next 1:1 unless otherwise noted.

### Every 6 weeks 1:1 (All members of marketing team with executive management)
{: .no_toc}

**Meeting goal: For CMO to help with any questions and career path discussion.**

**Run time: 30 minutes**

All members of the marketing team that are not direct reports should meet with their executive management (CMO) once every 6 weeks. If questions or concerns arise, please don't hesitate to reach out directly for an impromptu discuss via email or slack.

The meeting should run as follows:  

- First discuss any issues or problems that the manager can help with. Any roadblocks or disagreements?  
- Talk about career development and opportunities for growth.  
- Walk through quarterly OKRs to make sure they are relevant and see if help is needed.  
- Discuss upcoming OKRs if close to end of a quarter.  

### Monthly Marketing Townhall
{: .no_toc}

**Meeting goal: For the entire company to gain insight into what the Marketing team is focusing on.**

**Run time: 45 minutes**

The Monthly Marketing Townhall is open to the entire company for questions, ideas, and feedback. If you'd like to be added to this meeting, please contact anyone on the marketing team to be added to the invite.

To find the agenda for the bi-weekly marketing meeting, search the company Google Drive folder for "Monthly Marketing Townhall Agenda"

The meeting should run as follows:  

- 24 hours prior to the meeting all marketing members should have their sections of the agenda completed.  
- The CMO will email all attendees reminding them to take a look at the agenda and add any questions to the top of the meeting.  
- Meeting will open with the CMO giving an overview of new hires, organization changes, large upcoming campaigns, etc.  
- The meeting will then open to questions starting with the questions already documented.
- Everyone is encouraged to participate.    

### Monday & Wednesday Dinosaur party
{: .no_toc}

**Meeting goal: Dinosaur Party! (and also make sure we're all working together effectively)**

**Run time: 10-15 minutes**

The Monday and Wednesday Dinosaur Party is where the entire team meets for 10-15 minutes
immediately following the sales team call to discuss what we're all working on and to get
help with any questions. There is an agenda for this meeting, but it also should be an open forum.

The meeting should run as follows:

- First 60 seconds: Post funny GIFs in Slack `#marketing` room and person with funniest gets a dinosaur sticker. Person with most dinosaur stickers per quarter gets $250 bonus. Voting occurs with Smiley face emojis. You can only post one GIF per day
- Agenda items
- Everyone in order of Team Page (last goes first) gives 15 seconds about what they are working on. (Joe, Marcia, Colton, Amara, Ryan, Amanda, Braden, Mitchell, Ivan, Luke, Axil, Hank, Ashley, Emily)
- Discuss roadblocks and any collaboration needs

To find the Dinosaur Sticker leaderboard, search the company Google Drive for "Dinosaurs are awesome".

### Quarterly marketing review
{: .no_toc}

**Meeting goal: How did the last quarter go? What should we do for the upcoming quarter?**

**Run time: 120 minutes**

The Quarterly Marketing Review takes the place of the Monthly Marketing Meeting every last month of the new Quarter.

CMO works with the functional group leads to review planning for each group.

The meeting should run as follows:

- CMO leads review of over marketing strategies of last quarter. What worked? What didn't work? What should we do more of?  
- Head of Product Marketing reviews upcoming goals and plans for product marketing. Floor opens for discussion and brainstorming.  
- Head of Developer Relations reviews upcoming goals and plans for product marketing. Floor opens for discussion and brainstorming.  
- Head of Demand Generation reviews upcoming goals and plans for product marketing. Floor opens for discussion and brainstorming.  
- Head of Design reviews upcoming goals and plans for product marketing. Floor opens for discussion and brainstorming.  
- Team agrees on what key takeaways are from the last quarter.  
- Team agrees on what our Team OKRs should be for the upcoming quarter.  

## <i class="fa fa-thumbs-o-up fa-fw icon-color font-awesome" aria-hidden="true"></i> Marketing team SLAs (Service Level Agreements)
{: #sla}

When working remotely in such a quick moving organization, it is important for a team to agree on a few basic service level agreements on how we would like to work together. With anything, things can come up that make it not possible to meet these SLAs but we all agree to use best effort when possible.    

- Respond to your emails by end of next business day.
- Respond when you are cc'd with an action item on issues by end of next business day.  
- Be on time to meetings. We start at on time.  
- Acknowledge receipt of emails (community@, FYIs) by BCC'ing the list.
- Try not to email co-workers on weekends. Try out [Boomerang](http://www.boomeranggmail.com/) and set all your emails to send Monday morning at 6 AM. People will think you're up and working early! Time off is important. We all have stressful weeks so please unplug on the weekends where possible.
- Do not ping someone in a public channel on Slack on the weekends. This is rude.

## <i class="fa fa-file-code-o fa-fw icon-color font-awesome" aria-hidden="true"></i> Marketing Handbook Updates
{: #handbook}

Anything that is a process in marketing should be documented in the Marketing Handbook.

- Format of all pages should be as follows:  
    - Welcome to the Handbook.
    - Functional group overview if handbook for entire functional group or organization.
    - "On this page" index of all top level headers on the current page ([create a ToC]).
    - Links to other handbooks included on this page.
- Rather than create many nested pages, include everything on one page of your role's handbook with an index at the top.
- Each role should have a handbook page.
- If more than one person are performing a role, the task should be shared to update the handbook with all process or guidelines.
- Follow the [Markdown Style Guide] for about.GitLab.com.

## <i class="fa fa-rocket fa-fw icon-color font-awesome" aria-hidden="true"></i> How to contact marketing
{: #contact-marketing}

- [**GitLab Marketing public issue tracker**](https://gitlab.com/gitlab-com/marketing/issues/); please use confidential issues for topics that should only be visible to team members at GitLab
- You can also send an email to the Marketing team (see the "GitLab Email Forwarding" google doc for the alias).
- [**Chat channel**](https://gitlab.slack.com/archives/marketing); please use the `#marketing` chat channel for questions that don't seem appropriate to use the issue tracker or the internal email address for.

### <i class="fa fa-slack fa-fw slack font-awesome" aria-hidden="true"></i> Slack Marketing channels
{: #chat}

We use Slack internally as a communication tool. The marketing channels are as follows:  

- Marketing - This is the general marketing channel. Don't know where to ask a question? Start here.
- Advertising - Online marketing channel
- BDR team - For the BDR team
- Blog - Questions about the blog? This is your place.
- CFP - All call for speakers will be posted here.
- DevRel - A channel for the developer relations team to collaborate.
- Docs - Technical writing and documentation questions? This is your room.
- Events - Everything you want to know about events.
- GitLab Pages - Where we discuss everything related to GitLab Pages.
- Marketo Users - Having issues with Marketo? Ask here first.
- Newsletters - Watch our twice monthly newsletter get made here.
- Social - Twitter, Facebook, and other social media questions?
- Webcasts - Watch the webcast behind the scenes.
- Support - Ask any and all technical questions here for a quick response.
- SFDC users - Having issues with SFDC? Ask here first.

### <i class="fa fa-paper-plane-o fa-fw email font-awesome" aria-hidden="true"></i> Marketing email alias list
{: #email}

- Community@gitlab.com is an external email address that goes to the CMO, Field Marketing Manager, Senior Product Marketing Manager, and Demand Generation team.  
- Marketing@gitlab.com is an internal team email address for everyone on the marketing team.  
- Press@gitlab.com directs to the CMO and Senior Product Marketing Manager.  
- News@gitlab.com is an external email address for sending newsletters that goes to the Online Marketing Manager and Senior Demand Generation Manager.  
- Securityalerts@gitlab.com is an external email address for sending security alerts that goes to the Online Marketing Manager and Senior Demand Generation Manager.

<!-- IDENTIFIERS -->

[cmo]: https://about.gitlab.com/jobs/chief-marketing-officer/
[create a ToC]: /handbook/marketing/developer-relations/technical-writing/markdown-guide/#table-of-contents-toc
[Markdown Style Guide]: /handbook/marketing/developer-relations/technical-writing/markdown-guide/
[techwhirl.com]: http://techwhirl.com/what-is-technical-writing/

<!-- HANDBOOKS -->

[Blog]: /handbook/marketing/blog/
[Business Development]: /handbook/marketing/demand-generation/business-development/
[Content Marketing]: /handbook/marketing/product-marketing/content-marketing/
[Demand Generation]: /handbook/marketing/demand-generation
[Developer Advocacy]: /handbook/marketing/developer-relations/developer-advocacy/
[Developer Relations]: /handbook/marketing/developer-relations
[Design]: /handbook/marketing/design/
[Field Marketing]: /handbook/marketing/demand-generation/field-marketing/
[Markdown Guide]: /handbook/marketing/developer-relations/technical-writing/markdown-guide/
[Marketing Operations]: /handbook/marketing/demand-generation/marketing-operations/
[Online Marketing]: /handbook/marketing/demand-generation/online-marketing/
[Partner Marketing]: /handbook/marketing/product-marketing/partner-marketing/
[Product Marketing]: /handbook/marketing/product-marketing
[Social Media Guidelines]: /handbook/marketing/social-media-guidelines/
[Technical Writing]: /handbook/marketing/developer-relations/technical-writing/

<!-- Marketing Team: GitLab.com Handle -->

[amanda]: https://gitlab.com/u/afolson
[amara]: https://gitlab.com/u/amara
[ashley]: https://gitlab.com/u/AshleyS
[axil]: https://gitlab.com/u/axil
[braden]: https://gitlab.com/u/BradenC
[colton]: https://gitlab.com/u/coltontaylor
[emily]: https://gitlab.com/u/emilykyle
[hank]: https://gitlab.com/u/hanktaylor
[ivan]: https://gitlab.com/u/inem
[joe]: https://gitlab.com/u/joelucas
[luke]: https://gitlab.com/u/lukebabb
[marcia]: https://gitlab.com/u/virtuacreative
[mitchell]: https://gitlab.com/u/mitchellwright
[ryan]: https://gitlab.com/u/rycap

<!-- EXTRA STYLES APPLIED FOR THIS PAGE ONLY -->

<style>
.purple {
  color: rgb(107,79,187) !important;
}
.orange {
  color:rgb(252,109,38) !important;
}
.md-page h2 i.icon-color {
  color: rgb(107,79,187)
}
.md-page h2:nth-of-type(even) i.icon-color{
  color:rgb(252,109,38);
}
.font-awesome {
  font-size: .70em;
  vertical-align: middle;
  padding-bottom: 5px;
}
.btn-purple {
  color: rgb(107,79,187);
  background-color: #fff;
  border-color: #403366;
}
.btn-purple:hover {
  color: #fff;
  background-color: rgb(107,79,187);
  border-color: #403366;
}
.btn-purple-inv {
  color: #fff;
  background-color: rgb(107,79,187);
  border-color: #403366;
}
.btn-purple-inv:hover {
  color: rgb(107,79,187);
  background-color: #fff;
  border-color: #403366;
}
.btn-orange {
  color: rgb(252,109,38);
  background-color: #fff;
  border-color: rgb(226,67,41);
}
.btn-orange:hover {
  color: #fff;
  background-color: rgb(252,109,38);
  border-color: rgb(226,67,41);
}
.product.thumbnail img {
  display: block;
  max-width: 50%;
  margin: 20px auto;
}
.thumbnail img {
  display: block;
  max-width: 30%;
  margin: 20px auto;
}
.caption h4 {
  text-align: center;
}
.mkt-box {
  padding-bottom: 10px;
  padding-top: 10px;
  cursor: pointer;
}
.mkt-box:hover {
  /*border-radius: 5px;*/
  box-shadow:0 1px 5px rgba(0,0,0,0.3), 0 0 2px rgba(0,0,0,0.1) inset;
}
.mkt-row {
  padding-top: 20px;
  padding-bottom: 5px;
}
.mkt-row a:focus {
  outline: none;
}
.modal-header h2 {
  margin-top: 0;
}
.modal-footer p {
  margin-bottom: 0;
}
.center {
  text-align: center;
  display: block;
  margin-right: auto;
  margin-left: auto;
}
.description {
  color: #999;
}
.alert-purple {
  color: rgb(107,79,187);
  background-color: #fff;
  border-color: rgba(107,79,187,.5);
}
ul.toc-list-icons,
ul.toc-list-icons li ul {
  list-style-type: none;
  padding-left: 25px;
}
ul.toc-list-icons li ul {
  padding-left: 35px;
}
ul.toc-list-icons li i,
ul.toc-list-icons li ul li i {
  padding-right: 15px;
  color: rgb(107,79,187);
}
.slack {
  color: rgb(224,23,101);
}
.email {
  color: rgb(192,0,0);
}
ul.toc-list-icons li:nth-of-type(even) i {
  color:rgb(252,109,38);
}
ul.toc-list-icons li ul li i.slack {
  color: rgb(224,23,101);
}
ul.toc-list-icons li ul li i.email {
  color: rgb(192,0,0);
}
</style>
