---
layout: job_page
title: "UX Lead"
---

The User Experience (UX) Lead reports to the VP of Engineering, and UX
Designers report to the UX Lead.

## Responsibilities

* Interviews applicants for UX Designer
* Works with Frontend Lead to improve overall UX
* Sets overall direction of UX Design
* Reviews material made by UX designers
* Ensures that the technical decisions and processes set by the CTO are followed
* Does 1:1's with all reports every 2-5 weeks (depending on the experience of the report)
* Is available for 1:1's on demand of the report
* Ensures quality implementation of design materials
* Delivers input on promotions, function changes, demotions and firings in consultation with the VP of Engineering and Frontend Lead

## Requirements for Applicants

* A minimum of 3+ years experience as a design lead, product lead, or design manager
* Solid visual awareness with understanding of basic design principles like typography, layout, composition, and color theory
* Proficiency with pre-visualization software (e.g. Sketch, Adobe Photoshop, Illustrator)
* Experience defining the high-level strategy (the why) and creating design deliverables (the how) based on research.
* Passion for creating visually pleasing and intuitive user experiences.
* Collaborative team spirit with great communication skills
* You share our [values](/handbook/#values), and work in accordance with those values.
