---
layout: job_page
title: "Frontend Engineer"
---

## Responsibilities

* Implement the interfaces in GitLab proposed by UX Engineers and contributors
* Improve the [static website of GitLab](https://about.gitlab.com/) based on the suggestions of the Designer and CMO
* Continually improve the quality of GitLab

## Workflow

- You work on issues tagged with 'Frontend' on [CE](https://gitlab.com/gitlab-org/gitlab-ce/issues?label_name=Frontend) and [EE](https://gitlab.com/gitlab-org/gitlab-ee/issues?label_name=Frontend).
- The priority of the issues tagged with this label can be found in [the handbook under GitLab Workflow](https://about.gitlab.com/handbook/#prioritize).
- When done with a frontend issue remove the 'Frontend' label and add the next [workflow label](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/PROCESS.md#workflow-labels) which is probably the 'Developer' label.

## Requirements for Applicants

* Know how to use CSS effectively
* Know how to use JavaScript effectively
* Collaborate effectively with UX Designers, Developers, and Designers
* Be able to work with the rest of the community
* Needs to have extensive knowledge of Rails
* You share our [values](/handbook/#values), and work in accordance with those values.
